var searchData=
[
  ['score_5fbetween_5ftr_54',['score_between_tr',['../namespaceutils_d_t_w.html#aa48be35b02e8369b405da330053f896b',1,'utilsDTW']]],
  ['score_5fbetween_5ftrain_55',['score_between_train',['../namespaceutils_d_t_w.html#a6ee8cb534b4eac06b4031f002c4faec0',1,'utilsDTW']]],
  ['score_5fbetween_5ftrain_5fand_5ftest_56',['score_between_train_and_test',['../namespaceutils_d_t_w.html#ae2b14373c67d06acabb8c2b87a215c3d',1,'utilsDTW']]],
  ['score_5ftest_5ffile_57',['score_test_file',['../namespaceutils_d_t_w.html#aaf560a72b4b3dd70e389047e56634bcb',1,'utilsDTW']]],
  ['score_5ftest_5ffolder_58',['score_test_folder',['../namespaceutils_d_t_w.html#ac18d27723d2f547d330966ddfe2322ef',1,'utilsDTW']]],
  ['size_5fkeyword_59',['size_keyword',['../namespacereal__time.html#ab22ebf457ca465c7736ae55d2ee8e147',1,'real_time']]],
  ['stream_60',['stream',['../namespaceplay__file.html#a2b11d5cbf714c78bbad1d9c0ff060a44',1,'play_file.stream()'],['../namespacerecord__file.html#aa0f3947eb8f019ab293ec6061f35067c',1,'record_file.stream()']]]
];
