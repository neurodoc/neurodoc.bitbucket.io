var searchData=
[
  ['recognition_5ffile_94',['recognition_file',['../namespaceutils_d_t_w.html#ab3ba813813d2e5ab0b8b4327e5571d68',1,'utilsDTW']]],
  ['recognition_5ftest_95',['recognition_test',['../namespaceutils_d_t_w.html#a21f8cfc9d2a8a44818bd22595aa0df38',1,'utilsDTW']]],
  ['remove_5fsilence_96',['remove_silence',['../namespacefeatures.html#aa0dd2f81a1994fb1b78766a404642ee3',1,'features']]],
  ['run_97',['run',['../classreal__time_1_1recorder.html#abd6ab0e044104ff77f479d74329f0fc1',1,'real_time.recorder.run()'],['../classreal__time_1_1process.html#a6d949d62bb8fc292c7a5635341430eca',1,'real_time.process.run()'],['../classrealtime__v2_1_1recorder.html#abda7c4f6dbc2ae21606c22bf3a090ecd',1,'realtime_v2.recorder.run()'],['../classrealtime__v2_1_1process.html#abd081f6c27c40e0a61a490f2925e2c91',1,'realtime_v2.process.run()'],['../classrealtime__v3_1_1recorder.html#a7539106c5a70aec444e69933264950cc',1,'realtime_v3.recorder.run()'],['../classrealtime__v3_1_1process.html#a608345d889a0f65ba2b8991d5fb8672c',1,'realtime_v3.process.run()']]]
];
