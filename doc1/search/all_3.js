var searchData=
[
  ['data_5',['data',['../namespaceplay__file.html#a7b2b6593298c437474284d9e64d7873b',1,'play_file.data()'],['../namespacerecord__file.html#a6a286382dc68eaa216226d8b3d80c9b6',1,'record_file.data()']]],
  ['delta_6',['delta',['../namespacereal__time.html#a818769d2572d514effc0ba19ee14e620',1,'real_time.delta()'],['../namespaceutils_d_t_w.html#ab08bc48012aab9495a5bce96995dc488',1,'utilsDTW.delta()']]],
  ['done_5fyet_7',['done_yet',['../classrealtime__v3_1_1recorder.html#a650e9e7665eddc27b9b2f15990e64a49',1,'realtime_v3::recorder']]],
  ['dtw_8',['dtw',['../namespacedtw.html',1,'dtw'],['../namespacedtw.html#acff1e02a9877b2ce37ce755bc4e42b61',1,'dtw.dtw()']]],
  ['dtw_2epy_9',['dtw.py',['../dtw_8py.html',1,'']]]
];
