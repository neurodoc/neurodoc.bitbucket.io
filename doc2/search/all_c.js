var searchData=
[
  ['maxi_68',['maxi',['../namespaceserver.html#a479cd59cfc0e0953ce5d2f0d256b2926',1,'server']]],
  ['message_69',['message',['../classinterface_1_1_interface.html#a0041478d89eb9363e89a753444dffa99',1,'interface.Interface.message()'],['../classinterface_1_1_interface_training.html#a9cd99ce83a15defb6919162134d07230',1,'interface.InterfaceTraining.message()']]],
  ['metrics_70',['metrics',['../namespacecnn.html#a0fdd1cdd11f84f71ebdea4bbfecdab40',1,'cnn']]],
  ['model_71',['model',['../namespacecnn.html#a816463ba4bb830f5f68c5a6188f285f8',1,'cnn']]],
  ['model_5fjson_72',['model_json',['../namespacecnn.html#a99e42db5751ebdc555265593b4a919f2',1,'cnn']]],
  ['msgclient_73',['msgClient',['../namespaceserver.html#a30eabb5232f5aaf93fa4779b901f2fca',1,'server']]],
  ['msgserveur_74',['msgServeur',['../namespaceclient.html#a64f30ad36e76f42b2da53088f04d07a1',1,'client.msgServeur()'],['../namespaceserver.html#a02be546a36f732480b5eb9109e28f14e',1,'server.msgServeur()']]],
  ['mysocket_75',['mySocket',['../namespaceclient.html#aac5ab41ce94a3919ef588afa1acb384d',1,'client.mySocket()'],['../namespaceserver.html#a94dfe06e43f6c26cb015300ce10d0779',1,'server.mySocket()']]]
];
