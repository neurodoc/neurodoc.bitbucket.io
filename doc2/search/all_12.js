var searchData=
[
  ['scores_102',['scores',['../namespacecnn.html#aaab26a5407730c19ee196aa6b1e73047',1,'cnn']]],
  ['server_103',['server',['../namespaceserver.html',1,'']]],
  ['server_2epy_104',['server.py',['../server_8py.html',1,'']]],
  ['setup_105',['setup',['../namespacesetup.html',1,'']]],
  ['setup_2epy_106',['setup.py',['../setup_8py.html',1,'']]],
  ['short_5fnormalise_107',['SHORT_NORMALISE',['../namespacerecorder.html#a1d4862f8b35da869f13355d3ad3aede2',1,'recorder']]],
  ['split_5fdata_108',['split_data',['../namespaceutils.html#a0101a4e1d14bdb25fa47a14d93324f56',1,'utils']]],
  ['sr_109',['sr',['../namespaceserver.html#a9e4a89817826b679a5539428db4a062e',1,'server']]],
  ['start_110',['start',['../classinterface_1_1_interface.html#a9e06c99122be164a58f4dd4aaaf7955e',1,'interface.Interface.start()'],['../classinterface_1_1_interface_listening.html#a6922c6662a74f78a7da079e04a105701',1,'interface.InterfaceListening.start()'],['../classinterface_1_1_interface_training.html#a5f4b8be423aeff87eebb7a524590fb9d',1,'interface.InterfaceTraining.start()']]],
  ['stream_111',['stream',['../classrecorder_1_1_recorder.html#aa6e698099bd1f57a9b090d809f7a66a9',1,'recorder::Recorder']]]
];
