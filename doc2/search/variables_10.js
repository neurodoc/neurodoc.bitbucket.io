var searchData=
[
  ['target_250',['target',['../namespacecnn.html#aec811c23778906d89358597676e1853b',1,'cnn']]],
  ['textclasses_251',['textClasses',['../classinterface_1_1_interface.html#a99d55b1eb85b62150497863ba3851a38',1,'interface.Interface.textClasses()'],['../classinterface_1_1_interface_training.html#a20147453e440b524d3c91111849004e1',1,'interface.InterfaceTraining.textClasses()']]],
  ['time_5fout_5frecording_252',['TIME_OUT_RECORDING',['../namespaceclient.html#a1f158ccd6edd57326fdd71b684ee2a57',1,'client.TIME_OUT_RECORDING()'],['../namespacedataset__creation.html#a95ecb4a5c50da658f0ac1c61580b4d63',1,'dataset_creation.TIME_OUT_RECORDING()'],['../namespacerecorder.html#a8f777abc89c84f1389889f95a4d96604',1,'recorder.TIME_OUT_RECORDING()']]],
  ['train_5fdir_253',['train_dir',['../namespacecnn.html#ace722f512134c945601962464ab9a8ae',1,'cnn']]],
  ['treshold_254',['TRESHOLD',['../namespaceclient.html#ac16c621f5fc7b12e20d325e157596b52',1,'client.TRESHOLD()'],['../namespacedataset__creation.html#a13b616107e908e04cc40420a6f489a99',1,'dataset_creation.TRESHOLD()'],['../namespacerecorder.html#a87d9d3ad7db33bf73d3c484ff11b107c',1,'recorder.TRESHOLD()'],['../namespaceserver.html#a8c985a377624846471a6b420a369c4ff',1,'server.TRESHOLD()']]],
  ['txt_255',['txt',['../classinterface_1_1_interface_listening.html#ac240ba93a3afeac5184284805c018ee0',1,'interface::InterfaceListening']]]
];
