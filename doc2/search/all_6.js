var searchData=
[
  ['figsize_38',['figsize',['../namespacecnn.html#a9c5d53555be1ff58b632e5f553174b56',1,'cnn']]],
  ['finish_39',['finish',['../classinterface_1_1_interface.html#acddb46b58b44c52ef36fcbb5a50ea21f',1,'interface.Interface.finish()'],['../classinterface_1_1_interface_training.html#ae44b8b1d0dbeb09d3cdd4a5045f1926a',1,'interface.InterfaceTraining.finish()']]],
  ['fontstyle_40',['fontStyle',['../classinterface_1_1_interface.html#a7f2367a8d82e1fae3d0413a8cd21933d',1,'interface.Interface.fontStyle()'],['../classinterface_1_1_interface_listening.html#a3d37d5da7591ada50108282caa28322c',1,'interface.InterfaceListening.fontStyle()'],['../classinterface_1_1_interface_training.html#a63f5dd254101fe248f1d17897ef8e1a7',1,'interface.InterfaceTraining.fontStyle()']]],
  ['format_41',['FORMAT',['../namespaceclient.html#a99900a63709083d95312535c12a76de0',1,'client.FORMAT()'],['../namespacedataset__creation.html#a6ace19ab9314ed3c051bdc6f041a7a8c',1,'dataset_creation.FORMAT()'],['../namespacerecorder.html#a066e41617999d408cabd0c258d14d1e7',1,'recorder.FORMAT()']]]
];
