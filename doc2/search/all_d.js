var searchData=
[
  ['n_76',['n',['../namespacecnn.html#a4034aed1c236ad0f55cd31a7d8f97d6a',1,'cnn']]],
  ['name_77',['name',['../namespacesetup.html#ab3a7a0638d76a01367c5bc3cc699447f',1,'setup']]],
  ['nb_5fclasses_78',['nb_classes',['../namespacecnn.html#a6e10faa1bd4e169829ec71fc8d86a7ce',1,'cnn']]],
  ['next_79',['next',['../classinterface_1_1_interface.html#ad43797727815934a74cc178fed4a9bfa',1,'interface.Interface.next()'],['../classinterface_1_1_interface_training.html#a339290c51c3cd0f140970c4ca679f083',1,'interface.InterfaceTraining.next()']]],
  ['number_5ffile_5fper_5fdir_80',['NUMBER_FILE_PER_DIR',['../namespacecnn.html#afe57d7b64b3da7f3406e63be1aa54d9a',1,'cnn']]],
  ['numberdata_81',['numberData',['../classinterface_1_1_interface.html#ac69c2e8a9a07d2acecf4c3f885718082',1,'interface.Interface.numberData()'],['../namespacedataset__creation.html#a67b84b8d5de5be49e7f6b71fc8300de0',1,'dataset_creation.numberData()']]]
];
