var searchData=
[
  ['base_5fconfusion_5fmatrix_3',['base_confusion_matrix',['../namespaceutils.html#a3cdcc51e6cc2ee6fdd73a14275c3eace',1,'utils']]],
  ['base_5fdir_4',['base_dir',['../classinterface_1_1_interface_training.html#a050a7bf021d62fe6498840acb25c5999',1,'interface.InterfaceTraining.base_dir()'],['../namespacecnn.html#a188fa4557e9ae6185cc9ce5fa220cd41',1,'cnn.base_dir()']]],
  ['block_5',['block',['../namespacecnn.html#add86d214d2c17a7eee8b666e4f9e8edd',1,'cnn']]],
  ['btrain_6',['bTrain',['../namespacecnn.html#a0e207907c29c58695a784bbfa5f4f437',1,'cnn']]],
  ['bvalid_7',['bValid',['../namespacecnn.html#a6568a814609f690ee18cf448b281e272',1,'cnn']]]
];
