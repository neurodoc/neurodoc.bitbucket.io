var searchData=
[
  ['i_205',['i',['../namespaceclient.html#a966792a00b0ced1c64e2fbe323a566a3',1,'client.i()'],['../namespacecnn.html#a6a69f14ed884e6e67543473746597242',1,'cnn.i()']]],
  ['idx_5fto_5fclasses_206',['idx_to_classes',['../namespacecnn.html#acdc8ad992bd1ef02093f0e065fcae92f',1,'cnn']]],
  ['img_207',['img',['../namespacecnn.html#a641534c56a5887fee0d067888c4fdbd2',1,'cnn.img()'],['../namespaceserver.html#a0406f5eeaf20012f9b0170948a4f71dd',1,'server.img()']]],
  ['img_5fheight_208',['img_height',['../namespacecnn.html#aacda4a215e24e63bbbe8dbf3b04e6dac',1,'cnn.img_height()'],['../namespaceserver.html#acd809c51cd0758bc776bb2aed1c538a7',1,'server.img_height()']]],
  ['img_5fwidth_209',['img_width',['../namespacecnn.html#a09d4ee6e1235683866426245eb423000',1,'cnn.img_width()'],['../namespaceserver.html#a907852f69dd61b04ba5cfc8af73d1269',1,'server.img_width()']]],
  ['indmaxi_210',['indMaxi',['../namespaceserver.html#a32984101dc6cf38b753bf72439998fb2',1,'server']]],
  ['interface_211',['interface',['../namespacecnn.html#a6ae47851343dd2f421aeb04735c437e8',1,'cnn.interface()'],['../namespacedataset__creation.html#ab5ebfdf9cc1267ebad11fd2e567679d3',1,'dataset_creation.interface()']]],
  ['interfacelistening_212',['interfaceListening',['../namespacedataset__creation.html#a1a9f976ccb23200b1b3489af731944cf',1,'dataset_creation']]],
  ['isend_213',['isEnd',['../classinterface_1_1_interface.html#ae49fd0992872e5a0756c8a1d028c2856',1,'interface.Interface.isEnd()'],['../classinterface_1_1_interface_training.html#a420f409a54ee057c4372e4e79ec5076d',1,'interface.InterfaceTraining.isEnd()']]]
];
