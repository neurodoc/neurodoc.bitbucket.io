var searchData=
[
  ['p_84',['p',['../classrecorder_1_1_recorder.html#ab35e2cb32320b3da825e47d315718957',1,'recorder::Recorder']]],
  ['percent_85',['percent',['../classinterface_1_1_interface_training.html#a39e5db89b5327fe1bba78cc34fa661a0',1,'interface::InterfaceTraining']]],
  ['percent_5ftrain_86',['PERCENT_TRAIN',['../namespacecnn.html#a081cf7af432d718d2b676dcc236ad9b0',1,'cnn']]],
  ['percenttrain_87',['percentTrain',['../classinterface_1_1_interface_training.html#ae9d31046805d3fc4fa1204c608716d97',1,'interface::InterfaceTraining']]],
  ['port_88',['PORT',['../namespaceclient.html#acd2f280864b8945bb46e196ed2b0990a',1,'client.PORT()'],['../namespaceserver.html#a1cba9810471be10b671d686c0fe2d137',1,'server.PORT()']]],
  ['prediction_89',['prediction',['../namespaceserver.html#a8ca78ca965eff04301b0058fb875fc6d',1,'server']]],
  ['predictions_90',['predictions',['../namespacecnn.html#a686971dc773f33f5a5822d62a8f8d00b',1,'cnn']]]
];
